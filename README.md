Valawhole
=========

Valawhole is a simple 8/15 puzzle game, developed as an experiment using Vala and CSS-styled GTK+ 3.12.

# Current features

As the application is just a learning project, it isn't feature-rich, but it is feature-complete, with its minimal set of features:
* 3x3 and 4x4 mode
* keyboard and/or mouse control
* counting the moves
* colorful image (credits to kenney.nl)

# Known issues

There are some issues I am aware of, but didn't really spend the time necessary to fix them, yet.
* keyboard control sometimes misses a keypress
* shuffling is random 10-steps shuffling, so sometimes the random shuffling moves the puzzle close to the initial position

# Screenshots

![New game screenshot](https://lh6.googleusercontent.com/-i2VqHenNrrw/U1VxFJ-u_TI/AAAAAAAAFDE/5_5BiEk9-5g/w694-h527-no/valawhole-start.png)
![3x3 game screenshot](https://lh3.googleusercontent.com/-ZuEOuWxOag4/U1VxFI3r3xI/AAAAAAAAFC8/blrwkX7nQ5o/w694-h527-no/3x3.png)
![4x4 game screenshot](https://lh3.googleusercontent.com/-aojiqcWH9bk/U1VxFLOP6PI/AAAAAAAAFDA/PHiy2Fr0Lok/w694-h527-no/4x4.png)
