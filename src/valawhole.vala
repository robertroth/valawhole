/*
 * Copyright (C) 2014 Robert Roth
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 2 of the License, or (at your option) any later
 * version. See http://www.gnu.org/copyleft/gpl.html the full text of the
 * license.
 */

private class BoardView : Gtk.Grid
{
     private Gtk.Button[] buttons;

     private Board _board;
     public Board board
     {
        get { return _board; }
        set
        {
            var board = value;
            forall ( (child) => {remove (child);});
            buttons = new Gtk.Button[board.size*board.size-1];
            var buttons_css_provider = new Gtk.CssProvider ();
            string css = ".puzzleGrid .button { background-size: %d%%;background-image:url('%s');}\n".printf(board.size * 100, Path.build_filename (DATA_DIRECTORY, "castle.png") );
            css += ".completed { background-image: url('%s'); background-size:100%;}".printf (Path.build_filename (DATA_DIRECTORY, "castle.png"));
            for (int i=0; i < board.size; i++)
            {
                for (int j=0; j < board.size; j++)
                {
                    css += ".%d { background-position: %d%% %d%%}\n".printf(i*board.size + j, (100/(board.size-1)*j), (100/(board.size-1)*i));
                }
            }
            try
            {
                buttons_css_provider.load_from_data (css, -1);
            }
            catch (GLib.Error e)
            {
                warning ("Failed loading the custom-built css: %s", e.message);
            }
            Gtk.StyleContext.add_provider_for_screen (Gdk.Screen.get_default (), buttons_css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            for (int i=0;i<board.size*board.size-1;i++) {
                buttons[i] = new Gtk.Button();
                buttons[i].label = i.to_string ();
                buttons[i].show();
                buttons[i].get_style_context ().add_class (i.to_string ());
                attach (buttons[i], i%board.size, i/board.size, 1, 1);
                buttons[i].clicked.connect ( (button) =>
                {
                    int x = -1 , y = -1;
                    for (int k = 0; k < board.size; k++)
                    {
                        for (int j = 0; j < board.size; j++)
                        {
                            if (get_child_at (k, j) == button)
                            {
                                x = k;
                                y = j;
                            }
                        }
                    }
                    board.move (x, y);
                });

            }
            board.moved.connect (moved_cb);
            board.completed.connect (completed_cb);

            _board = board;
        }
    }

    public BoardView ()
    {
        set_events (Gdk.EventMask.KEY_PRESS_MASK | Gdk.EventMask.KEY_RELEASE_MASK);
    }

    public override bool key_press_event (Gdk.EventKey event)
    {

        int direction = -1;
        switch (event.keyval)
        {
        case Gdk.Key.Left:
        case Gdk.Key.h:
            direction = 2;
            break;

        case Gdk.Key.Right:
        case Gdk.Key.l:
            direction = 3;
            break;

        case Gdk.Key.Up:
        case Gdk.Key.k:
            direction = 0;
            break;

        case Gdk.Key.Down:
        case Gdk.Key.j:
            direction = 1;
            break;

        default:
            return false;
        }
        if (direction == -1)
            return false;

        board.move_free (direction);

        return true;
    }

    public void moved_cb (int x, int y, int to_x, int to_y)
    {
        var to_move = get_child_at (x, y);
        remove (to_move);
        attach (to_move, to_x, to_y, 1, 1);
    }
    
    public void completed_cb ()
    {
        forall ( (child) => { remove (child); });
        get_style_context ().add_class("completed");
    }
}

private class Board : Object
{
    private int[,] _values;

    public signal void moved(int x, int y, int to_x, int to_y);
    public signal void completed();

    private int _moves;
    public int moves
    {
        get { return _moves;}
    }
    private int _free_x;
    private int _free_y;

    private bool _started = false;

    public int free_x
    {
        get { return _free_x;}
    }

    public int free_y
    {
        get { return _free_y;}
    }

    private int _size;

    public int size
    {
        get { return _size;}
    }

    public Board (int size)
    {
        _values = new int[size, size];
        for (int i = 0; i < size * size - 1; i++)
            _values[i/size, i%size] = i;
        _size = size;
        _free_x = _free_y = size - 1;
        _values [_free_x, _free_y] = - 1;
        _moves = 0;
    }

    public void shuffle (int steps)
    {
        _started = false;
        int count = 0;
        int direction;
        int last_direction = 10;
        while (count < steps)
        {
            direction = GLib.Random.int_range (0, 4);
            //stdout.printf ("last : %d, current : %d, diff : %d, categorylast : %d, category_current: %d\n", last_direction, direction, (last_direction - direction).abs (), last_direction / 2, direction / 2);
            if (last_direction / 2 == direction / 2 && (last_direction - direction).abs () == 1)
                continue;
            if (move_free (direction))
                count++;
            last_direction = direction;
        }
        _moves = 0;
        _started = true;
    }

    public void move (int x, int y)
    {
        if ((x-free_x).abs () + (y-free_y).abs () != 1)
            return;
        if (_started)
            _moves ++;
        moved (x, y, _free_x, _free_y);
        _values[_free_y, _free_x] = _values[y, x];
        _free_y = y;
        _free_x = x;        
        _values[_free_y, _free_x] = -1;
        if (_started && done ())
            completed ();
    }

    public bool done ()
    {
        for (int i=0; i < _size*_size - 1;i++)
            if (_values[i/_size, i%_size] != i)
                return false;
        return true;
    }

    public bool move_free (int direction)
    {
        int x = _free_x;
        int y = _free_y;
        switch (direction)
        {
            case 0: // up
            y++;
            break;
            case 1: // down
            y--;
            break;
            case 2: // left
            x++;
            break;
            case 3: // right
            x--;
            break;
            default:
            break;
        }
        /** If there is a tile in the given direction from the free tile*/
        if (x >= 0 && x < _size && y >= 0 && y < _size )
        {
            //stdout.printf ("Moving (%d, %d) %s\n", y, x, direction==0?"up":direction==1?"down":direction==2?"left":"right");
            move (x, y);
            return true;
        }
        return false;
    }

}

public class Valawhole : Gtk.Application
{
    private Menu app_main_menu;

    private int SHUFFLE_COUNT = 10;

    /* Main window */
    private Gtk.Window window;

    private BoardView main_grid;
    private Board board;
    private Gtk.HeaderBar headerbar;
    private Gtk.Box overlay;
    private Gtk.Button play;
    private Gtk.ToggleButton[] difficulties;

    private const GLib.ActionEntry[] action_entries =
    {
        { "new-game",           new_game_cb                                   },
        { "quit",               quit_cb                                       },
        { "about",              about_cb                                      }
    };

    public Valawhole ()
    {
        Object (application_id: "org.evfool.valawhole", flags: ApplicationFlags.FLAGS_NONE);
    }

    protected override void startup ()
    {
        base.startup ();

        Environment.set_application_name (_("Valawhole"));

        Gtk.Window.set_default_icon_name ("valawhole");

        var base_css_provider = new Gtk.CssProvider ();
        var base_css_path = Path.build_filename (DATA_DIRECTORY, "valawhole.css");
        try
        {
            base_css_provider.load_from_path (base_css_path);
        }
        catch (GLib.Error e)
        {
            warning ("Error loading css styles from %s: %s", base_css_path, e.message);
        }
        Gtk.StyleContext.add_provider_for_screen (Gdk.Screen.get_default (), base_css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        add_action_entries (action_entries, this);

        app_main_menu = new Menu ();
        app_main_menu.append (_("_New Game"), "app.new-game");
        app_main_menu.append (_("_About"), "app.about");
        app_main_menu.append (_("_Quit"), "app.quit");
        set_app_menu (app_main_menu);

        add_accelerator ("<Primary>n", "app.new-game", null);
        add_accelerator ("<Primary>w", "app.quit", null);
        add_accelerator ("<Primary>q", "app.quit", null);

        window = new Gtk.ApplicationWindow (this);
        window.title = _("Valawhole");
        window.icon_name = "valawhole";
        window.set_size_request (400, 400);

        headerbar = new Gtk.HeaderBar ();
        headerbar.show_close_button = true;
        headerbar.set_title (_("Valawhole"));
        headerbar.show ();
        window.set_titlebar (headerbar);

        add_window (window);
        
        var container = new Gtk.Overlay ();
        container.show ();
        container.expand = true;

        var aspect_frame = new Gtk.AspectFrame ("", 0.5f, 0.5f, 1.0f, false);
        aspect_frame.border_width = 24;
        aspect_frame.set_shadow_type (Gtk.ShadowType.NONE);
        aspect_frame.show ();
        aspect_frame.expand = true;;
        
        main_grid = new BoardView ();
        main_grid.get_style_context ().add_class ("puzzleGrid");
        main_grid.row_homogeneous = true;
        main_grid.row_spacing = 0;
        main_grid.column_spacing = 0;
        main_grid.column_homogeneous = true;
        main_grid.expand = true;
        main_grid.show ();

        aspect_frame.add (main_grid);
        container.add (aspect_frame);
        window.add (container);

        overlay = new Gtk.Box (Gtk.Orientation.VERTICAL, 6);
        overlay.expand = true;
        overlay.get_style_context ().add_class ("overlayBox");
        container.add_overlay (overlay);
        overlay.show ();
        play = new Gtk.Button.from_icon_name ("media-playback-start-symbolic", Gtk.IconSize.DIALOG);
        play.show ();
        play.expand = false;
        play.halign = Gtk.Align.CENTER;
        play.valign = Gtk.Align.CENTER;
        play.clicked.connect ( () => { new_game_cb (); overlay_switch_cb (false); });
        play.get_style_context ().add_class ("playButton");
        container.add_overlay (play);

        difficulties = new Gtk.ToggleButton[2];
        difficulties[0] = new Gtk.ToggleButton.with_label ("3 x 3");
        difficulties[0].active = true;
        difficulties[0].show ();
        difficulties[1] = new Gtk.ToggleButton.with_label ("4 x 4");
        difficulties[1].show ();
        difficulties[0].get_style_context ().add_class ("3x3");
        difficulties[1].get_style_context ().add_class ("4x4");
        difficulties[0].get_style_context ().add_class ("diff");
        difficulties[1].get_style_context ().add_class ("diff");
        difficulties[1].bind_property ("active", difficulties[0], "active", GLib.BindingFlags.INVERT_BOOLEAN | GLib.BindingFlags.BIDIRECTIONAL);
        difficulties[0].set_size_request (120, -1);
        difficulties[1].set_size_request (120, -1);
        var box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
        box.margin_top = 12;
        box.show ();
        box.pack_start (difficulties[0], true, true, 0);
        box.pack_start (difficulties[1], true, true, 0);
        box.expand = false;
        box.halign = Gtk.Align.CENTER;
        box.valign = Gtk.Align.CENTER;
        box.get_style_context ().add_class ("linked");
        overlay.pack_start (box, false, false, 6);
        overlay.border_width = 0;
        new_game_cb ();
        main_grid.completed_cb ();
    }

    public void start ()
    {
        window.show ();
    }

    public void overlay_switch_cb (bool show)
    {
        play.visible = show;
        overlay.visible = show;
    }
    public override void activate ()
    {
        window.show ();
    }

    private void quit_cb ()
    {
        window.destroy ();
    }

    private void new_game_cb ()
    {
        //stdout.printf ("New game\n");
        main_grid.get_style_context ().remove_class("completed");
        board = new Board (difficulties[0].active ? 3 : 4 );
        main_grid.board = board;
        update_moves_cb ();
        board.moved.connect (update_moves_cb);
        board.completed.connect ( () => {overlay_switch_cb (true); });
        board.shuffle (SHUFFLE_COUNT);
    }

    private void update_moves_cb ()
    {
        headerbar.set_subtitle (ngettext ("%d move", "%d moves", board.moves).printf (board.moves));
    }

    private void about_cb ()
    {
        string[] authors =
        {
            "Robert Roth",
            null
        };

        string[] artists =
        {
            "Kenney.nl",
            null
        };

        Gtk.show_about_dialog (window,
                               "name", _("Valawhole"),
                               "version", VERSION,
                               "comments",
                               _("Sliding puzzle game"),
                               "copyright",
                               "Copyright © 2014 Robert Roth",
                               "license-type", Gtk.License.GPL_3_0,
                               "authors", authors,
                               "artists", artists,
                               "translator-credits", _("translator-credits"),
                               "logo-icon-name", "valawhole", "website",
                               "https://wiki.gnome.org/RobertRoth/Valawhole",
                               null);
    }

    public static int main (string[] args)
    {
        Intl.setlocale (LocaleCategory.ALL, "");
        Intl.bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
        Intl.bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
        Intl.textdomain (GETTEXT_PACKAGE);

        var context = new OptionContext (null);
        context.set_translation_domain (GETTEXT_PACKAGE);
        context.add_group (Gtk.get_option_group (true));

        try
        {
            context.parse (ref args);
        }
        catch (Error e)
        {
            stderr.printf ("%s\n", e.message);
            return Posix.EXIT_FAILURE;
        }

        var app = new Valawhole ();
        return app.run ();
    }
}
